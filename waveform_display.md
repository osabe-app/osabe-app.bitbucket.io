# Introduction

Displaying a waveform graphically is an integral part of this WAV editor; doing so in performant and flexible manner was a bit of a challenge, and took a few interations to refine. While the task of displaying a waveform isn't anything novel, I couldn't find too much on how to actually accomplish it when I searched online; so in this post I'm going to go into how waveform rendering is implemented in **osabe** to give a leg up to anyone who's looking to do something similar.

There are two aspects to rendering I'll discuss: drawing a static waveform and smoothly moving a it across the screen while scrolling.


# Drawing a Static Waveform

## Premise

Most voice-over audio these days will likely be recorded at a minimum rate of 44,100 samples per second, or CD-quality. Twenty seconds of audio then will contain nearly a million samples, and twenty minutes of audio would be nearly 53 million samples. Trying to draw that many samples multiple times per second is a non-starter for current consumer hardware (based on experimentation).

Thankfully we don't have to try to draw that many samples at once, because we can't _see_ that many samples at once.

Say we're zoomed in on an audio waveform to a segment about half a second in length, or ~22k-samples' worth. Since the majority of the waveform is out of view at this level of zoom, we don't have to worry about rendering it; the part of the waveform that we can see on the screen will be spread out over a display-width of a couple thousand pixels, with each pixel encompassing about ten samples' worth of data. At this density it's feasible to render every single sample contained in the segment, and it's fairly necessary we do so because each sample will contribute to what's seen on the screen (fig x).

![img]()(fig x)

If we zoom out on the audio then the area occupied by the same half-second timespan on the screen will be compressed down to a few or eventually just a single pixel in width. At this level of zoom we won't be able to discern the fine-grained details embodied by every single sample, but the display will be dominated by a few extereme miniumum/maximum values that eclipse the rest (fig x). Thus, even though what we're displaying on the whole screen will encompass magnitudes more data, we only have to display a few well-chosen samples for each segment of audio to display the whole waveform.

![img]()(fig x)

So, we either have to display many samples for a small segment of the waveform, or a small number of select samples for many segments.

The former task is pretty straight-forward, so we won't spend much time on it here; the latter task is where the challenge is found: how do we select which samples to display at any level of zoom and how do we store them in a manner that yields flexible yet efficient retrieval? (There's also the matter of transitioning from one display method to the other, but that ends up being fairly trivial.)




When showing a small portion of the overall waveform on the screen, say half a second of audio or ~22k-samples' worth, it will be spread over a display-width of a couple thousand pixels and each pixel will encompass about ten samples' worth of data. At this scale we can easily see the sinuous crests and troughs of the waveform within that timespan and each individual sample will contribute to what is seen on the screen (fig x).
[Since this is only a small fragment of the overall data, we can get away with showing all the samples that lie within it and everything we display contributes to it.]

![img]()(fig x)

As we zoom out on the audio then the area occupied by the same half-second timespan on the screen will be compressed down to a few or eventually just a single pixel in width, and at that point we don't need to draw all of the samples in the audio for that timespan to display it, just a few representative minimum and maximum values to give an overall impression of the waveform's shape at that point (fig x); even though what we're displaying on the whole screen when zoomed out encompasses magnitudes more data, we can't see the fine-grained details of it so we'll only be showing a miniscule representation on screen of all the data in view.

![img]()(fig x)

So we can see either many details of a small portion of the waveform, or few details of many portions; this leads to two methods of display: showing either individual samples or pairs of representative minimum/maximum extremes.

Both methods require writing a maxiumum of tens of sample per pixel width to the screen to render the waveform, which is definitely feasible on current hardware, so it might seem like there's no issue with drawing waveforms, but the challenge comes in deciding which extremes to display when we're zoomed out and storing them in a manner that yields flexible yet efficient retrieval. (There's also the matter of transitioning from one display method to the other, but that's fairly trivial.)

[]
The data-structure I ended up designing arose from the constraints I had for the application, so let's go over those first, then we'll derive the solution.

## Constraints

The first constraint of this application was zooming: I wanted to be able to quickly change the amount of visual information that was displayed for the waveform when zooming in/out; I'd also planned on displaying the waveform not just in the main editing area, zoomed in to a specific timespan, but also in the view control, showing the full waveform, so retrieval of extreme-pairs would have to be efficient enough to support both views / zoom levles.

[NEED TO INTRODUCE CONCEPT OF CLIPS PRIOR TO THIS!?]
The second constraint was that of segmentation: each clip of audio in the interface can be split -- either when scrolling in and out of view or interactively by the user -- and, in addition, clip portions can be moved around in time, separated from other portions of the same clip. Any extremes holding structure would have to support easy arbitrary data access for different portions of a clip.

Third, given the amount of data in a file this structure would have to be efficient in both storing the values and retrieving them.

[]After tooling around with a few different ideas (including trying to use a Hilbert transform to calculate a function that would give me an envelope for the waveform), what I ended up using was derived mostly from the first of the two constraints above.

## Derivation

If we look at a clip at maximum zoom-out, it will occupy just one pixel's width on the screen, and what is displayed will be the greatest magnitude positive and negative samples contained within its timespan.

![img]()?

As we start to zoom in, the next representation will occupy two pixels' width on the screen, and the extremes that were shown in the one-pixel wide representation will both shift to one half or the other of the new waveform view; the other positive and negative halves will be filled in by the greatest extremes of those newly exposed halves of the clip. (Note that these new extremes won't necessarily be the next-greatest positive and negative extremes in the entire clip, since those might reside in the same halves already represented by the greatest extremes, and be thus eclipsed by them.)(figure x)

![img]()(fig x)

After each successive widening of the clip's display width, new extremes will appear in the newly exposed, more-localized timespans that are no-longer represented by the previous higher-magnitude extremes, so smaller and smaller extremes will become visible over time (until the point at which the display logic switches over to showing pure samples rather than extremes). (Note that zooming in won't necessarily double the width of the representation on screen each step, so the number of extremes exposed each time should be considered a flexible factor when designing the data-structure.)

From this progression we can see two things: the first is that those extremes with the greatest magnitudes should generally be given higher priority of access in whatever data-structure we use to store them, since they're going to be used most often, and second that we're going to need to be able to find the greatest extreme representing any arbitrary section of a clip (thereby making the second constraint outlined earlier redundant).

After wrestling for a while with various ideas (using pre-sorted lists, trying to create an envelope function using the Hilbert transform, et al.) the data-structure I finally arrived at for storing the extremes is as follows:

Create a binary tree with the highest magnitude extreme of a clip as its root value, along with an index indicating the extreme's time-location within the clip. The child-nodes to the left and right of the root should then contain the next-highest magnitude extremes in the clip before and after the root in time, respectively, along with their time indices.

For each successive node, assign its left and right sub-nodes to be the next highest extremes between the node and its ancestors or the edge of the clip, as appropriate. Repeat this process for each node to build out the rest of the tree. (fig x)

![img]()(fig x)

Two such trees should be built for each clip: one for the positive extremes in the clip and one for the negative extremes.

To retrieve data from a tree, specify a timespan for which you'd like an extreme. Traverse down the tree checking whether the time index of each node you encounter falls within the interval of the timespan; if it does then you have your extreme value; if it doesn't, then you move to the left or the right of the node depending on which side of the time interval its index falls on.


time complexity: probably generally somewhere between `O(h)` and `O(log_2 n)`, though it's possible to have a worst-case of `O(n)` for something like a low-frequency saw-tooth or triangle wave. (Excuse my lack of confidence on this matter, I was an EE major :D)


can do display extremes for each clip to cut down on the data to keep -- no point in doing it for a gap

eventually will want to switch to displaying actual samples, so don't need to keep the full range of samples in the extremes structure. Decided to partition the full range of samples from a clip into chunks of 32 samples each and select the representative minimum and maximum values from each chunk to use in the extremes data-structure. (So the time index stored with each extreme will actually be a chunk index rather than a sample index.)
