<html>
  <head>
    <link rel="stylesheet" type="text/css" href="styles/main.css" />
    <link rel="icon" type="image/ico" href="images/favicon.ico" />
    <title>UI Screens: Architecture-Level Functions (2023-06-29) - osabe blog</title>
  </head>
  <body>
    <header>
      <nav>
        <a href="2022-02-07_optimization.html">&#171; Optimization</a>
        <a href="index.html">Index</a>
        <a href="#"></a>
      </nav>
      <hr/>
      <p>
        <h1>UI Screens: Architecture-Level Functions</h1>
        <em>Published: 2023-06-29</em><br />
        <em>Edited: 2023-08-14</em>
      </p>
    </header>
    <article>
      <section>
        <p>In previous posts the arcitecture of this application was related: a Model-View-Presenter atop a stack of monads:</p>
        <img src="images/2021-07-08_architectural_refinements/state.png" alt="A Model-View-Presenter triad (horizontally) atop a stack of monads (IO, ReaderT, StateT, and Pipes)"/>
        <p>This setup felt clean-enough initially given the size of the application, but some of the implementaiton details belied its suitability as the top level of abstraction. The main issue, as providentially revealed, was that it was mixing different levels of architectural function into a single layer, muddying things.</p>
        <p>In this post I'm going to go over what was wrong with this architecture and what the fix was.</p>
      </section>
      <section>
        <h2>Issues and a Potential Solution</h2>
        <p>The user-interface of this application currently consists of three different UI screens:</p>
        <ul>
          <li><strong>Idle</strong> which instructs the user to drag-and-drop a WAV file into the program for it to load</li>
          <li><strong>Loading</strong> to show the progress of reading the file from disk and other processing</li>
          <li><strong>Editing</strong> to let the user manipulate the WAV file's contents</li>
        </ul>
        <p>Each screen represents a particular stage of operation in the app -- prompting, loading, or interactivity -- with the processing associated with a stage being done by the Model in the background.</p>
        <p>The Model and Presenter of the MVP triad were implemented as state-machines, with each machine consisting of states from all three of the different UI screens:</p>
        <img src="images/2023-06-29_screens/states.png" alt="The distinct screens' states within the Presenter and Model of the architecture"/>
        <p>The fact that both machines had states for each of the three screens is where the problem lay; it meant that:</p>
        <ul>
          <li>the states for one screen had to be concerned with ignoring or handling Model-Presenter messages meant for the other screens <sup><a name="ref1ret" href="#ref1">1</a></sup></li>
          <li>the states had to be cognizant of some of the implementation details of the other screens' states for transitioning to them when the application switched screens</li>
          <li>the two state-machines had to be kept in sync so they were both functioning in states for the same screen</li>
        </ul>
        <p>All of this added to the messiness and complexity of the code for these modules.</p>
        <p>A way to improve this situation, removing these detractive qualities, seemeed to be to split the states out by screen and create an MVP triad for each to separate their concerns:</p>
        <img src="images/2023-06-29_screens/triads.png" alt="New architecture with a Model-View-Presenter triad and monad-stack for each screen" />
        <p>At first I was uncertain of how sound a decision this might be architecturally, but then something presented itself that quelled my doubts.</p>
      </section>
      <section>
        <h2>Architecture-Level Functions</h2>
        <p>To transition between the UI screens in the shared-state-machine architecture meant transitioning from one screen's state to another screen's in both the Model and the Presenter. If I were to separate the screens out into different MVP triads it would mean that such direct state-transitions would no-longer be possible when changing screens since their states would be in different Models and Presenters.</p>
        <p>Furthermore, the data passed from state to state as part of these transitions would have to go through the new intermediary layer of the MVP triads whenever the app changed screens; whatever mechanism for screen-transitions I came up with would have to facilitate this data transfer.</p>
        <p>It was while mulling over this detail w.r.t. the <strong>Loading</strong> screen that I had a moment of discernment -- one of many divine insights received during this proceess.</p>
        <p>The <strong>Loading</strong> screen represents the stage of the application concerned with reading and processing a WAV file, so its triad would have to be given the path of a file to load, which it would do in the background, and it would have to return the loaded file and associated data for use by the next screen when it was done.</p>
        <p>If you squint and look at this description in the abstract, <em>it describes a function!</em> It takes an input, does something, and returns an output! In fact, the application stages represented by <em>every</em> screen could be thought of in terms of being functions:</p>
        <ul>
          <li>the <strong>Idle</strong> screen would take no arguments and return the path of a file the user gave it -- equivalent to a <code>getLine</code>!</li>
          <li>the <strong>Loading</strong> screen would accept a file path and return the loaded file and visualization data, or an error</li>
          <li>the <strong>Editing</strong> screen would accept the loaded file and visualization data for the user to edit and have no return value <sup><a name="ref2ret" href="#ref2">2</a></sup></li>
        </ul>
        <p>With this insight I had my abstraction, along with the realization that the screen-transition logic belonged on its own architectural layer -- so far as the overarching application flow is concerned, displaying stuff to the user and accepting UI input in each screen are just implementation details; the high-level screen-to-screen flow is functional: after a function (screen) is run, the next function to be called depends on the particular output received from the previous one.</p>
        <p>Amid other, ongoing refactoring, I moved each screen's state-logic into its own MVP triad and wrapped it as an effectful function, then setup some simple logic to perform the transitions between them:</p>
        <code>
          <pre>
mainLoop configFilePaths settingsFilePath files = do
  (config, messages) &lt;- readConfigFiles configFilePaths
  graphics &lt;- initializeGraphicsContext (getStyle config) programName
  let context = GlobalContext programName config graphics
  _ &lt;- nextScreen context =&lt;&lt;
    if null files
    then idleScreen context messages
    else loadingScreen context messages (head files)
  destroyGraphicsContext graphics

nextScreen context = \case
  GotFile path -&gt; do
    ret &lt;- loadingScreen context [] path
    nextScreen context ret
  FileLoadErrored{} -&gt; do
    ret &lt;- idleScreen context []
    nextScreen context ret
  FileLoaded audio segments waveRep -&gt; do
    ret &lt;- editingScreen context [] audio segments waveRep
    nextScreen context ret
  Close -&gt;
    return ()
          </pre>
        </code>
      </section>
      <section>
        <h2>Conclusion</h2>
        <p>This solution segregated the screen-transition logic from the state-machines, extracting it to its own level of abstraction, making it cleaner and clearer. The rest of the code is now more cohesive, with better separation of concerns -- a definite win. (I did have to sacrifice the ability to dynamically reload the style at will with this change, but it was a hacky feature implementation-wise and needed to be rethought-out, which I'm still working on; I'll take the cleaner code for sure.)</p>
        <p>I'm not sure if this solution is an already-established pattern for application architecture/flow which I've just rediscovered or if it's something new, but I thought I'd write about it since I could see it being handy for others; I think it might work well in the realm of videogames or mobile apps to represent the different menu screens a user can go through -- the menu item they select would be the return value of the screen-function they're on.</p>
      </section>
      <section>
        <h1>Acknowledgements</h1>
        <ul>
          <li>God for the insights and everything else</li>
          <li><a href="https://github.com/fasiha/" target="_blank">Ahmed Fasih</a> for reviewing a draft of this post</li>
        </ul>
      </section>
    </article>
    <footer>
      <hr/>
      <nav>
        <a href="2022-02-07_optimization.html">&#171; Optimization</a>
        <a href="index.html">Index</a>
        <a href="#"></a>
      </nav>
      <hr/>
      <section>
        <h1>Footnotes</h1>
        <ol>
          <li><a name="ref1"></a>At the end of the post on <a href="2021-07-08_architectural_refinements.html" target="_blank">architectural refinements</a> this point was lamented -- it was a code-smell -- but I didn't know how to handle it at that time.<br/><a href="#ref1ret">Return to text &#8593;</a></li>
          <li><a name="ref2"></a>Each screen can additionally return a new file path or a quit event, depending on what action the user takes, but the Edit screen doesn't have a main return value associated with its core functionality.<br/><a href="#ref2ret">Return to text &#8593;</a></li>
        </ol>
      </section>
    </footer>
  </body>
</html>
