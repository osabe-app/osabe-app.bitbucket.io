<html>
  <head>
    <link rel="stylesheet" type="text/css" href="styles/main.css" />
    <link rel="icon" type="image/ico" href="images/favicon.ico" />
    <title>Case Study: Abstracting Threading (2021-05-19) - osabe blog</title>
  </head>
  <body>
    <header>
      <nav>
        <a href="2021-05-19_abstraction.html">&#171; Abstraction</a>
        <a href="index.html">Index</a>
        <a href="2021-06-07_architecture.html">Application Architecture &#187;</a>
      </nav>
      <hr/>
      <p>
        <h1>Case Study: Abstracting Threading</h1>
        <em>Published: 2021-05-21</em><br/>
        <em>Edited: 2021-06-16, 2021-09-09</em>
      </p>
    </header>
    <article>
      <section>
        <p>There were a few issues that I encountered while abstracting threading that I thought might be of interest, so I'll discuss them here.</p>
        <p>The first was finding the right abstraction, the second was implementing <em>any</em> threading abstraction.</p>
      </section>
      <section>
        <h2>Finding the Right Abstraction</h2>
        <p>In my initial approach to abstracting threading, I was focused on code reuse. I'd already defined a mutable-state abstraction to hold the application's shared state and thought to reuse that to hold the progress updates and results of long-running threaded operations -- with a separate typeclass to abstract actually running something in a thread.</p>
        <p>Here's some pseudo-code for what I'd defined:</p>
        
        <code>
          <pre>
data State r
  = Waiting
  | Running Float
  | Cancelled
  | Errored String
  | Completed r

class Monad a =&gt; Container a where
  create :: state -&gt; a var
  read :: var -&gt; a state
  update :: var -&gt; (state -&gt; state) -&gt; a ()
  -- `state` contains either the operation's progress or result

class Monad a =&gt; Operation a where
  runInThread :: a () -&gt; a ()
          </pre>
        </code>
  
        <p>These interfaces seemed to be ok, their concerns were pretty orthogonal and the threading code could run operations regardless of whether they updated their progress or not -- a state variable to hold progress updates could be passed to the function being run in a thread separately as an argument.</p>
        <p>But when I started implementing things, this abstraction didn't feel right; I was trying to do too many things in the wrong places.</p>
        <p>For one thing, I'd been thinking to use the state variable to control the thread from the outside -- setting the <code>Cancelled</code> state externally would tell the thread to quit -- thus using the same channel that the function would send its output on as an input. But that's too much for one thing to do; I was unable to ensure that the output of the function wouldn't overwrite a control signal in, and vice-versa. (I'm ashamed of the na&iuml;vet&#233; this approach displays, as I actually have a decent amount of good threading experience and should know better.)</p>
        <p>Disjoining the input and output responsibilities into separate <code>State</code> types and passing in two different state variables to a threaded method would have been cleaner, but it still wouldn't have ameliorated everything else that was wrong.</p>
        <p>Passing a state variable to a threaded function <em>at all</em> was problematic because <strong>(a)</strong> it polluted what could otherwise have been pure code with the knowledge and use of the state abstraction and <strong>(b)</strong> it added the onerous responsibility of keeping track of the thread run-state to a function that already had its own work to get done.</p>
        <p>As I was wrestling with these disadvantageous qualities, the following shifts in thinking took place:</p>
        <ol>
          <li>Have the threaded method return its output <em>as a return-value</em>, not via a state variable (duh)</li>
          <li>Move threading control (i.e. cancellation) into the <em>threading abstraction</em> instead of making the threaded function care about it and check a state variable for instructions</li>
          <li>Move progress-reporting into the threading abstraction (since I was only going to be threading operations that I wanted to know the running status of anyway)</li>
          <li>Hide the progress-reporting mechanism within the threading abstraction and pass a monadic function to the threaded method for it to call to report progress updates</li>
        </ol>
        <p>Point <strong>3</strong> meant I could avoid having to pass around and keep track of a state variable <em>and</em> a thread handle in external code, and point <strong>4</strong> meant the threaded function could be otherwise essentially pure code (so easier to test).</p>
  
        <p>After making these changes, I arrived at the following abstraction:</p>
        <code>
          <pre>
data Status r
  = Completed r
  | Errored SomeException
  | Progressing Float -- 0-1 of progress

class Monad m =&gt; Operation m o | m -&gt; o where
  start :: ((Float -&gt; m ()) -&gt; m r) -&gt; m (o r)
  cancel :: o r -&gt; m ()
  poll :: o r -&gt; m (Status r)
          </pre>
        </code>
  
        <p>If the type-declaration of the <code>start</code> method seems confusing, it's just saying that the function that's going to be threaded has to take as an argument a monadic function it should call with progress updates, which the abstraction implementation will provide to it.</p>
      </section>
      <section>
        <h2>Implementing an Abstraction</h2>
        <p>With a good abstraction established, I thought things would move forward smoothly thenceforth. However, when I went to implement it, one detail proved problematic:</p>
        <p>Threads in Haskell, so far as I've seen at least, can only be run explicitly in one of two base monads: <code>IO</code> or <code>ST</code>.</p>
        <p>The base monad of our <code>App</code> type, which is what we're abstracting from, is already <code>IO</code>, so this felt like a non-issue, but since the threading abstraction was being implemented at the <code>App</code> level, the function being passed in to run in a thread would be of the <code>App</code> type as well, and not of type <code>IO</code>.</p>
        <p>Whoops.</p>
        <p>In order run a method of type <code>App</code> in an <code>IO</code> thread, I'd have to somehow go through the <code>ReaderT GlobalContext</code> that encased <code>IO</code> in the <code>App</code> type to get at the base monad.</p>
        <p>Thankfully there are a couple of libraries to do just that; I ended up choosing to go with the <a href="https://hackage.haskell.org/package/monad-unlift-0.2.0">monad-unlift</a> library since it lets one unwrap from either <code>IO</code> <em>or</em> <code>ST</code>, and I thought I might want to use the latter in testing.</p>
        <p>Here's the code to implement the abstraction:</p>
  
        <code>
          <pre>
data IORunState r
  = IORunState
    { asyncOf :: A.Async r
    , progressOf :: IORef Float
    }

instance Operation App IORunState where
  start op = do
    ioRef &lt;- liftIO (newIORef 0)
    let report = liftIO . writeIORef ioRef
    UnliftBase runInBase &lt;- askUnliftBase
    -- libs we're using appear to need `forkOS` to be used,
    -- which `asyncBound` uses
    async &lt;- liftIO (Async.asyncBound (runInBase (op report)))
    return (IORunState async ioRef)
  cancel = liftIO . Async.cancel . asyncOf
  poll (IORunState a r) = do
    status &lt;- liftIO (Async.poll a)
    case status of
      Nothing -&gt; do
        p &lt;- liftIO (readIORef r)
        return (Progressing p)
      Just (Left e) -&gt;
        return (Errored e)
      Just (Right v) -&gt;
        return (Completed v)
          </pre>
        </code>
  
        <p>I was mildly surprised when this code actually compiled and ran without issue.</p>
        <p>This code creates a schism in the monadic stack between the code in the main thread and the code in the function being run in a child thread, with the only remaining connection between them being the <code>IO</code> monad. With this schism, only the return value from the threaded function or effects in <code>IO</code> will become available to the caller; any modifications made to other parts of the thread-local copy of the monadic stack by the child thread will be lost once that thread has completed. Since the progress updates are written to an <code>IO</code> state variable behind the scenes, in-thread calls to the progress-reporting function will be visible to the outside world.</p>
        <p>A few notes:
          <ul>
            <li>I decided to change from using <code>IO</code> threads directly to using the <code>Control.Concurrent.Async</code> module, since it supports easier cancellation, but <code>IO</code> is still used to run the threads underneath it</li>
            <li><a href="https://hackage.haskell.org/package/monad-unlift-0.2.0">The readme of the monad-unlift library</a> says that the library has some ways to make coordinating logic between threads more convenient, if more communication between threads is desired, but I'm just using an <code>IORef</code> for progress reporting</li>
          </ul>
        </p>
  
        <p>Continuing with the <a href="2021-05-18_foundation.html#monad-metaphor">building metaphor from the Foundation post</a>, this implementation would be equivalent to having two buildings each built on a common ground floor; the child building would have a copy of the information in the parent building's levels, but any modifications made in the child building (e.g. on a <code>StateT</code> level) wouldn't make it over to the same level in the parent building unless some extra work was done to shuttle it across on the ground floor.</p>
      </section>
    </article>
    <footer>
      <hr/>
      <nav>
        <a href="2021-05-19_abstraction.html">&#171; Abstraction</a>
        <a href="index.html">Index</a>
        <a href="2021-06-07_architecture.html">Application Architecture &#187;</a>
      </nav>
    </footer>
  </body>
</html>
