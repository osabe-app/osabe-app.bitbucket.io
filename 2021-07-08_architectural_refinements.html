<html>
  <head>
    <link rel="stylesheet" type="text/css" href="styles/main.css" />
    <link rel="icon" type="image/ico" href="images/favicon.ico" />
    <title>Architectural Refinments (2021-07-08) - osabe blog</title>
  </head>
  <body>
    <header>
      <nav>
        <a href="2021-06-07_architecture.html">&#171; Application Architecture</a>
        <a href="index.html">Index</a>
        <a href="2021-12-07_frp.html">Functional Reactive Programming &#187;</a>
      </nav>
      <hr/>
      <p>
        <h1>Architectural Refinements</h1>
        <em>Published: 2021-07-08</em><br/>
        <em>Edited: 2021-09-09</em>
      </p>
    </header>
    <article>
      <section>
        <p>In the last post the basic architecture of the application-level code was developed:</p>
        <img src="images/2021-06-07_architecture/architecture_mvp.png" alt="The View, Presenter, and Model modules connected together using Pipes, all on top of the application base (IO and ReaderT)" />
        <p>In this post we'll dive a bit deeper into the state-machines then go over some of the pragmatic refinements made to the architecture and to the implementing code.</p>
      </section>
      <section>
        <h2>State-Machines</h2>
        <p>This application has a few distinct phases of operation:</p>
        <ul>
          <li><strong>Idle</strong> for when the application is waiting to be told what file to load by the user</li>
          <li>One of various <strong>loading</strong> stages while the file is loaded and processed</li>
          <li><strong>Editing</strong> for when the user is able to interact with the audio data</li>
        </ul>
        <p>In order to keep the code coherent and manageable, the architectural components -- specifically the Model and the Presenter -- were broken down into different states corresponding to each of these operational phases, thus becoming state-machines.</p>
        <p>The states of these machines are comprised of one or more functions each, with each machine then being a set of mutually-recursive functions.</p>
        <p>As the codebase grew, many things were done to keep the management of these functions tenable. This is what we'll discuss in this post.</p>
      </section>
      <section>
        <h2>State Monad</h2>
        <p>Many of the functions in the View and Presenter components rely on shared state, but to pass that state around as a discrete argument to each function would have been burdensome. Therefore, I decided to try adding state monad-transformers to the mix to carry the state along under the hood.</p>
        <p>My desire was to have distinct states/transformers for each of the components, to keep their concerns separate. At first I wasn't sure if this was possible; it turns out it is, though there is a little catch.</p>
        <p>My first attempt at adding state monad-transformers looked like this:</p>
        <img src="images/2021-07-08_architectural_refinements/state_bad.png" alt="The attempted addition of the State monad on top of the IO, ReaderT and Pipes monad stack" />
        <p>As you might guess, it didn't work so well.</p>
        <p>While the type-system happily allowed this and the program ran, memory consumption ballooned like mad.</p>
        <p>I would like to be able to explain the technical reasons behind this, and I think I have an intuitive idea, but my monad-transformer-fu is not up to snuff. (As I mentioned in an earlier post, I'm still learning and this project was experimental for me.)</p>
        <p>At first I was worried about what this would mean for the feasibility of the project, but after a little digging a solution presented itself (what a blessing!): the pipes library has facilities for running other monad transformers <em>beneath</em> it, so all I had to do was swap the order of the transformers in the type definitions and run the state monads with <code>evalStateP</code> from <code>Pipes.Lift</code> instead of <code>evalStateT</code>.</p>
        <p>The architecture now looks like this:</p>
        <img src="images/2021-07-08_architectural_refinements/state.png" alt="The correct addition of the State monad, underneath Pipes -- between it and ReaderT " />
        <p>and works just fine.</p>
      </section>
      <section>
        <h2>Alleviating Boilerplate</h2>
        <p>In <a href="2021-05-19_abstraction.html">the post on abstraction</a> I outlined some type-classes that were made to abstract from <code>IO</code> operations and configuration locations.</p>
        <p>For a function to use these type-classes they have to be declared as constraints in the type-signature of the function. With a state-machine consisting of many functions, each calling the others, and each function requiring multiple type classes, that led to a <em>lot</em> of constraint declarations.</p>
        <p>This became quite annoying, especially when adding a new constraint to the mix -- all of the functions of a machine had to be updated with the new constraint.</p>
        <p>To alleviate this I thought I might make a single type-class inheriting from all the others I needed for a machine, like so:</p>
        <code>
          <pre>
class
  ( HasSettings r
  , HasStyle r
  , HasProgramName r
  , MonadFix m
  , MonadReader r m
  , Operation m o
  ) =&gt; PresenterStateConstraint m r o where {}
          </pre>
        </code>
        <p>and then specify that single class on each of the machine's functions.</p>
        <p>As I started to implement this, though, I saw that it would require me to make an instance of the conglomerate type-class for the <code>App</code> type, and that seemed like unnecessary boilerplate since <code>App</code> already implemented each of the inherited type-classes individually.</p>
        <p>After a bit of research, I found one could create <a href="https://downloads.haskell.org/~ghc/7.0.1/docs/html/users_guide/type-class-extensions.html#undecidable-instances" target="_blank">class synonyms</a>: type-class instances not tied to any particular type! This meant I could avoid having to declare a specific instance for <code>App</code> -- <code>App</code> wouldn't even have to know anything about the state-machine type-classes -- and just have a general synonym instead.</p>
        <p>Here's the synonym instance for the above type-class, and it lives in the same module as the declaration, just beneath it in the code:</p>
        <code>
          <pre>
instance
  ( HasSettings r
  , HasStyle r
  , HasProgramName r
  , MonadFix m
  , MonadReader r m
  , Operation m o
  ) =&gt; PresenterStateConstraint m r o where {}
          </pre>
        </code>
        <p>(Note that we're declaring an <code>instance</code> here rather than a <code>class</code> as above; other than that the code is the same.)</p>
        <p>So now when a type-class has to be added to or removed from a machine, it's a change of as little as two lines!</p>
        <p>If you'd like to use class-synonyms you'll need to enable the <code>UndecidableInstances</code> language extension, and IIRC the <code>FlexibleInstances</code> extension as well.</p>
      </section>
      <section>
        <h2>Message-Type Refactoring</h2>
        <p>Communication between the different MVP components in this application is done via message passing, with each inter-component message channel having its own data-type:</p>
        <ul>
          <li><strong>Presenter Input</strong> for View&rarr;Presenter messages</li>
          <li><strong>Presenter Output</strong> for Presenter&rarr;View messages</li>
          <li><strong>Model Input</strong> for Presenter&rarr;Model messages</li>
          <li><strong>Model Output</strong> for Model&rarr;Presenter messages</li>
        </ul>
        <p>On the receiving side, these messages are processed in case expressions; e.g.:</p>
        <code>
          <pre>
case input of
  Message1 field1 field2 field3 -&gt;
    ...
  Message2 field1 field2 -&gt;
    ...
  Message3 field1 field2 field3 field4 field5 field6 field7 -&gt;
    ...
  ...
          </pre>
        </code>
        <p>With a lot of message constructors to process, these expressions were prone to being long and/or repetitive -- both undesirable traits. So when they became burdensome to work with, I decided to refactor.</p>
        <p>In my time programming in Haskell I've noticed that the structure of the data-types one declares has a pretty noticeable influence on the code that gets written, a phenomenon in evidence here.</p>
        <p>As part of the refactoring of this code some adjustments were made to a couple of the message data-types so that the code that handled them would be cleaner; we'll discuss these changes in the following sub-sections.</p>
        <h3>Data Segregation: Reducing Churn</h3>
        <p>One of the things I wanted to do when refactoring message handling in the Presenter was to separate the longer bodies of case expression matches out into discrete functions.</p>
        <p>These functions would have to be given the data from the message type-constructors the cases matched against, and that meant a potential for code churn: if a data-field was added to or removed from a message constructor, then the pattern in the case expression for that constructor would have to be adjusted and the type-signature and argument list of the processing function would have to change as well.</p>
        <p>During times of high fluidity this became annoying, so I took all the data-fields out of the constructor and put them into a separate product type, then made the constructor carry just that type. That way, when adjustments were made to the product type, only the producer and the consumer of that data would have to change, but all of the code in-between could stay the same, thus reducing the churn.</p>
        <h3>Generalized Messages: Eliminating Redundancy</h3>
        <p>Many of the messages sent to the Model from the Presenter invoke transformations on internal model data. Initially, I'd had separate message type-constructors corresponding to each of these transformations, with the message-handling code in the Model calling the appropriate transform functions.</p>
        <p>As the number of transforms grew, more and more message type-constructors were required and the code to do the message handling became quite redundant: each of the messages was essentially the arguments for a transformation function sans the internal model data, so all of the calls looked similar:</p>
        <code>
          <pre>
case input of
  DoTransform1 arg1 arg2 -&gt;
    update modelData (transform1 arg1 arg2)
  DoTransform2 arg1 arg2 arg3 -&gt;
    update modelData (transform2 arg1 art2 arg3)
  DoTransform3 arg1 -&gt;
    update modelData (transform3 arg1)
  DoTransform4 arg1 arg2 -&gt;
    update modelData (transform4 arg1 arg2)
  ...
          </pre>
        </code>
        <p>So, to refactor, I replaced all the different transform message type-constructors with one or two generalized constructors that accepted a function to run on the internal model data instead:</p>
        <code>
          <pre>
case input of
  DoTransform f -&gt;
    update modelData f
  ...
          </pre>
        </code>
        <p>This simplified the processing code and as a bonus I could use function composition to pass multiple transforms in as a single message, which came in handy when I started really implementing the UI logic.</p>
      </section>
      <section>
        <h2>Future Refinement</h2>
        <p>One style of Haskell development is to use the type system to express as much as possible about what a program does, leaving less chance for an error to sneak through in the data.</p>
        <p>I would like to use this style in the state-machines to express what inputs a particular state is expecting and exclude all the other inputs that might be acceptable to other states. However, I don't know if the pipes library is able to accomodate this. It's something I may attempt in the future, though, in one way or another.</p>
      </section>
    </article>
    <footer>
      <hr/>
      <nav>
        <a href="2021-06-07_architecture.html">&#171; Application Architecture</a>
        <a href="index.html">Index</a>
        <a href="2021-12-07_frp.html">Functional Reactive Programming &#187;</a>
      </nav>
    </footer>
  </body>
</html> 
