Some major UI refactoring was done for [v0.10.0 of osabe](https://www.osabe.app/versions/0.10/download.html), and I was granted a few insights during the process that I think are worth sharing.

The first was a way to handle adjustable GUI layouts in a functional manner, which I ended up spinning off in a library: the Functional UI Layout Engine (FULE; [github](https://github.com/pschnapp/FULE), [hackage](https://hackage.haskell.org/package/FULE)). The techinque is decently documented in the project's repo so I won't talk about it much more in this post.

The second was covered in the last post on [UI screens as functions](2023-06-29_screens.html).

The third insight I will disucss here.

In versions prior to v0.10.0 the application's GUI was controlled by a FRP network: the GUI layout was derived from the composition of the network, and the visual components were implemented as part of the state of the network; inputs to the network were user input from the View and the state of the Model, and the outputs from the network were updates for the Model, commands for the Presenter, and visuals for the View to reneder.

As mentioned in [the post about FRP](2021-12-07_frp.html), this worked decently well, but it was somewhat inflexible and was already starting to crack under the strain of the application's architecture, though I didn't recognize it as such at the time.

Fast-forward to me wanting to make a few changes, 

[what broke it was:]

1. I wanted to make improvements to what I've been calling the View Model[^1] for better visuals when scrolling the waveforms. This would involve adding an extra input to the FRP network so that the View Model could be fed in to the visual components that required it.

[^1]: This would probably be better-served with a different name, I just don't know what to call it. It's kind of a specific view of the Model, so maybe a Model View?

2. I wanted to switch to using the new layout technology I'd been working on, which meant keeping the layout in an external structure rather than as an integral part of the FRP nework. The layout is used by the visual components, so the network would need another input for it to be fed in, and the components can affect the layout, which meant another network output.

And since the View Model is generated from many of the same inputs the visual components use, for convenience I decided to make View Model generation a hidden UI component, which meant another output.

For those not keeping track, we have:

Inputs
 - Model state
 - View Model for components
 - Layout
 - User input from the View

Outputs
 - Updates for the Model
 - New View Models
 - Updates for the Layout
 - Messages for the Presenter
 - Visuals the View should render

An FRP network would be able to handle taking in these inputs and generating these outputs were it merely a case of a direct transformation, but that's not the case in this app.

need to make distinction between network and visual components and show that the vis comps required all the inputs and produced the outputs


This was apparent early on, I just didn't have the perspective to really apprecaite it.
[] let's look at the update cycle for the Model and View.

The FRP network took in the Model and View as inputs, then generated an update for the Model, and a function to generate output for the View when it was fed the state of the Model.

The View output couldn't be generated directly by the network because the Model had to be updated first, and that used to generate the View. If the View weren't generated from the updated Model, it would be one step out of sync with the state of the Model and the application would feel laggy.

action had to be taken outside of the network before one of the network's outputs could be fully realized.

adding more [components] added more interdependencies:

[enumerate interdependencies]
viewmodle depends on layout and model
layout depends on output of componetns
model depends on user input and ?
view depends on layout and model and viewmodels

we can't do a single transformation from inputs to outputs because some outputs depend on intermediate steps

It was while I was trying to figure out how manage the ordering of all these inputs and outputs that it hit me: keep the components in a constant state of flux; feed them different inputs when they became available and extract outputs when they're needed. they'll be constantly churning
thought of chemical reaction as metaphor, with reactants being added and products being the outputs. Kept in a reactor (type wrapper for het types)


fule layout to be updatead and inputted
view models to be updatead end inputted

FRP is mostly input -> network -> output
had been keeping component state in network, so couldn't really get at it with multiple inputs and outputs happening at different times
hadd been able to cope with needing to update the model before generating the view by returning a function to update the model and a function to generate the view from the updated model as the output from the FRP network

