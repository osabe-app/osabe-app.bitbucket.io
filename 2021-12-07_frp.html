<html>
  <head>
    <link rel="stylesheet" type="text/css" href="styles/main.css" />
    <link rel="icon" type="image/ico" href="images/favicon.ico" />
    <title>Functional Reactive Programming (2021-12-07) - osabe blog</title>
  </head>
  <body>
    <header>
      <nav>
        <a href="2021-07-08_architectural_refinements.html">&#171; Architectural Refinements</a>
        <a href="index.html">Index</a>
        <a href="2022-02-07_optimization.html">Optimization &#187;</a>
      </nav>
      <hr/>
      <p>
        <h1>Functional Reactive Programming</h1>
        <em>Published: 2021-12-07</em>
      </p>
    </header>
    <article>
      <section>
        <p>In the last couple of posts the architecture of osabe WAV editor was developed: an MVP implementation atop a stack of monads:</p>
        <img src="images/2021-07-08_architectural_refinements/state.png" alt="The Model-View-Presenter triad atop a stack of monads (IO, ReaderT, StateT, Pipes)" />
        <p>In the introductory post of this blog Functional Reactive Programming (FRP) was also mentioned as being used in the UI, in conjunction with the MVP pattern. In this post we're going to go over why FRP was used and how the UI was implemented using it, at various levels, after which we'll do a little analysis.</p>
      </section>
      <section>
        <h2>Motivation</h2>
        <a href="https://osabe.app/images/screenshot.png" target="_blank"><img src="https://osabe.app/images/screenshot.png" style="border: none; float: left; width: 200px; margin-right: 20px; margin-bottom: 10px;" alt="Application screenshot" /></a>
        <p>Once the program was able to display a waveform, some rudimentary interactivity was added to allow panning of the view to test the waters of implementing a UI. This consisted of using a couple of state variables to keep track of the mouse's position and the depression state of the left mouse-button to determine whether the user was dragging and if so by how much, updating the Model appropriately.</p>
        <p>All of this state was kept in the Presenter (since that's where UI logic is supposed to reside in the MVP pattern).</p>
        <p>The panning functionality worked, but to continue implementing further interactivity using discrete state variables like this seemed like the wrong thing to do given the complexity of what had been envisioned for the UI -- I could just feel the impending burden of the code required.</p>
        <p>Over the years I'd done a bit of reading about FRP and had used stream-processing in OO languages and this situation seemed like an appropriate occasion to employ the technique.</p>
        <p>For those unfamiliar with it, reactive programming treats certain things, e.g. a mouse's position, as time-varying signals or streams. These streams can be transformed, split, combined, etc. to form a processing network that automatically reacts to changes in any of the input signals to produce a concerted output.</p>
        <p>In this application I figured the mouse's position, the mouse button states, meta-key states, and the window size could all be streams fueled by the user's input and the processing network of the UI would react to changes in these signals to update the Model and generate visual output for the View.</p>
        <p>While I wasn't sure at first how feasible it would be to implement UI components with it, FRP felt intuitively like the way to go, so I decided to try it and I'm pretty happy with how things turned out.</p>
      </section>
      <section>
        <h2>UI Implementation</h2>
        <p>For the most part in this blog we've been covering the construction of this application chronologically, but the development of the UI was kinda haphazard, so to relate the implementation here we're going to do a structured overview instead, starting at a high level and then working our way down:</p>
        <ul>
          <li><a href="#integration">FRP Integration with the MVP</a></li>
          <li><a href="#assembled">The UI as Assembled Components</a></li>
          <li><a href="#netwire">Interlude: netwire</a></li>
          <li><a href="#components">UI Components as State-Machines</a></li>
          <li><a href="#data-streams">Data Streams</a></li>
        </ul>
      </section>
      <section>
        <h3><a name="integration">FRP Integration with the MVP</a></h3>
        <p>Internally FRP is concerned with manipulating and using streams of data, but externally (for the implementation used in this application at least) it's just another stateful computation.</p>
        <p>State in functional programming is often simulated in a function by adding an extra input and having an extra output; the state the operation needs to access is passed in along with all of the other function inputs and a new state value is returned along with the actual output of the function that contains any modifications made to it during the operation:</p>
        <img src="images/2021-12-07_frp/state.png" alt="A simple state-ful computation with a state and input as the inputs and a new state and output as the outputs" width="40%" />
        <p>In our case, the state passed to the UI function is the stateful network of wired-together stream-processors and the inputs are the user's input from the View and a representation of the state of the Model. The UI operation feeds the View input and Model state to the network to drive its internal data streams, and the outputs from the network -- updates to run on the Model and a description of what to display in the View -- are returned from the function along with the new (post run) stateful network:</p>
        <img src="images/2021-12-07_frp/frp_state.png" alt="A state-ful computation in osabe's FRP, taking in the UI state, user input, and Model state as inputs and outputting a new UI state, display output, and Model update as outputs" width="75%" />
        <p>This operation can be integrated with the MVP components pretty directly, like so:</p>
        <a href="images/2021-12-07_frp/laggy_integration.png" target="_blank"><img src="images/2021-12-07_frp/laggy_integration.png" alt="The state-ful FRP computation integrated into the MVP triad in the Presenter; the user input and display output come from and go to the View, resepectively, the Model state and Model updates come from and to go the Model, respectively, and the UI state just loops back around to the computation input from the computation output" /></a>
        <p>While this setup looks ok and does work, there's one major issue with it: it's inherently laggy.</p>
        <p>To see why, let's consider an example:</p>
        <p>Suppose a user is panning the waveform around. The input from the View side would be a mouse movement message, and input from the Model would be a view of the waveform in state n. After the FRP network is run, the output would be a representation of the system in state n for the View to display and an update message to make the Model transition from state n to state n+1, to adjust the position of the view of the waveform.</p>
        <p>Wrapping back around to the top, the user is still panning so we get another input of mouse movement and we get a representation of the Model in state n+1; but here there's a mismatch: the output on the screen, which was generated from state n, is a step behind the current Model state of n+1 -- the user is seeing and reacting to outdated visual output! In fact, the user won't see the visual representation resulting from their previous action until after their current input is processed by the UI!</p>
        <p>To remedy this, we need a strategic delay: rather than generate the output for the View directly from our FRP network, we'll have it return a display-description-producing <em>function</em>. To produce a description of what to display in the View, then, we'll take the Model update outputted by the network and apply it to the Model, read the new state, pass that to the description-producing function, and finally send the description output back to the View, like so:</p>
        <a href="images/2021-12-07_frp/responsive_integration.png" target="_blank"><img src="images/2021-12-07_frp/responsive_integration.png" alt="The state-ful FRP computation with some changes to the wiring: the display output is generated from the FRP computation output as well as the state of the Model -- the Model update is applied and the new Model state is retrieved before the display output is generated to keep the View in sync with the Model" /></a>
        <p>Now the user gets more immediate feedback from their input and the UI feels more responsive.</p>
        <p>An alternative to using a delay here might be to run the Model update on the state that was passed to the UI operation before generating the output for the View, but then the update would have to be reapplied to the Model, duplicating the effort. Or, if this code were not purely functional then the UI operation might update the Model directly, in response to user input, before generating output for the View. But as things are, we're using a delay, imbuing this code with a good allocation of responsibilties and keeping it functionally pure.
      </section>
      <section>
        <h3><a name="assembled">The UI as Assembled Components</a></h3>
        <p>With this higher-level perspective in mind, let's step into the UI FRP network and consider that at a high level.</p>
        <p>The visual UI of this application is divided into discrete areas, each of which reacts to user input differently and produces a different output on the screen. In addition, the UI has other reactions to user input that may not be tied to any specific area of the screen, such as stopping playback.</p>
        <p>To realize these different behaviors we use distinct reactive stream-processing elements that we'll call <em>components</em> to transform the UI input stream into different output streams.</p>
        <p>Since there are two basic types of output a component might have -- either with or without visual content -- I've categorized the components of this application into one of two types: Panels or Controls.</p>
        <p>Panels take the network inputs and produce output for the View along with updates for the Model (called Feedback in the code) and Controls produce only Feedback for the Model and nothing for the View.</p>
        <p>Components are assembled together to form the UI network by replicating the network's input across them and combining their outputs into a single output for the network as a whole:</p>
        <img src="images/2021-12-07_frp/component_bundle.png" alt="The input to the FRP network is split out and goes through the components in parallel, then the outputs of the components are aggregated to become the output of the FRP network as a whole" />
        <p>(The final output of the network should obviously be a Panel type output.)</p>
        <p>The components used to form the UI can be atomic units or they can be compositions of other components, and how they're combined determines the layout and behavior of the UI.</p>
        <p>A few general functions were written to combine components of different types in various ways:</p>
        <ul>
          <li><code>fanThrough</code> simply replicates the input across a list of homogeneous components and combines their outputs.</li>
          <li><code>integrateFeedback</code> takes a Control and a Panel as arguments, routes the input through them both and combines their outputs together.</li>
          <li><code>layeredPanel</code> and <code>splitPanel</code> both take a number of Panel components, route the network input through them, then combine their outputs, accumulating the visual portions so they're situated on the screen correctly. <code>splitPanel</code> also modifies the input stream on its way to the Panels to implement things like mouse focus and to react to window size changes.</li>
        </ul>
        <p>Using just these functions a decently sophisticated UI can be assembled from components as can be seen in this code for the editing screen:</p>
        <code>
          <pre>
<em>{-# ANN module "HLint: ignore Redundant bracket" #-}</em>
editScreen
  :: (Monad m, MonadFix m)
  =&gt; Word -&gt; Word -&gt; PanelContext Model.Status -&gt; EditScreen m
editScreen resizeBarSize navBarHeight context = trackPanelContext context &gt;&gt;&gt;
  -- Lisp mode!
  (integrateFeedback
    (fanThrough
      [ autoscrollControl   <em>-- for scrolling the view along with the playback cursor</em>
      , pagingControl       <em>-- home, end, page-up, page-down reactions</em>
      , playbackControl     <em>-- play/pause</em>
      , quitControl         <em>-- self-explanatory</em>
      , stateControl ])     <em>-- for reacting to save and undo/redo inputs</em>
    (splitPanel (Fixed SizedBottom statusPanelHeight)
      (splitPanel (Expandable SizedBottom navBarHeight resizeBarSize)
        (viewPanel)         <em>-- waveform view; uses more combining functions internally</em>
        (viewControlPanel)) <em>-- view controller; uses more combining functions internally</em>
      (statusPanel)))
          </pre>
        </code>
        <p>(Note that a Lisp-style syntax was used here since the nested syntactic structure seems to better reflect the UI layout in code than a more Haskelly syntax did -- I'd tried using variables in a <code>where</code> clause but it felt less clear.)</p>
        <p>The result of the <code>editScreen</code> function is a processing network that can be applied to the UI's inputs to produce UI outputs, as shown in the previous section.</p>
      </section>
      <section>
        <h3><a name="netwire">Interlude: netwire</a></h3>
        <p>Before we continue with the main exposition, some knowledge of the FRP library used in this application should be introduced.</p>
        <p>The library I eventually settled on using is <a href="https://hackage.haskell.org/package/netwire" target="_blank">netwire</a> <sup><a name="ref1ret" href="#ref1">1</a></sup>, an arrowized functional reactive programming library. I chose it since it seemed to have most if not all of the features I'd need to create the UI. I'll try to explain everything you need to know about this library for you to follow along below, but if you'd like more information on or examples of it I found the following resources useful:</p>
        <ul>
          <li><a target="_blank" href="https://danbst.wordpress.com/2013/01/23/novice-netwire-user/">Novice netwire user</a></li>
          <li><a target="_blank" href="https://ocharles.org.uk/blog/posts/2013-08-01-getting-started-with-netwire-and-sdl.html">Getting started with netwire and SDL</a></li>
          <li><a target="_blank" href="https://ocharles.org.uk/blog/posts/2013-08-18-asteroids-in-netwire.html">Asteroids in netwire</a></li>
          <li><a target="_blank" href="https://todayincode.tumblr.com/post/96914679355/almost-a-netwire-5-tutorial">Almost a netwire 5 tutorial</a></li>
          <li><a target="_blank" href="https://todayincode.tumblr.com/post/98441034180/netwire-5-vs-elerea">Netwire 5 vs Elerea</a></li>
        </ul>
        <p>There are a few basic concepts you'll need to know to understand the content in the following sections:</p>
        <p>The stream-processing type of the netwire library is the <code>Wire</code>, representing a transform from an input stream to an output stream. This is the type that contains state in a network.</p>
        <p>In the code snippets below we'll be using a simplified type-signature for this type, <code>Wire m i o</code> where:</p>
        <ul>
          <li><code>m</code> is a monad</li>
          <li><code>i</code> is the type of value in the input stream</li>
          <li><code>o</code> is the type of value in the output stream.</li>
        </ul>
        <p>Streams/signals can come in one of two flavors in netwire: regular value streams (which are continuous), or <em>events</em> (with which there can be data or no data on a stream at any point in time). The code for this UI uses both, sometimes simultaneously.</p>
        <p>Another concept that I'll mention but that I don't think you'll need to know for this discussion is <em>inhibition</em>, which is when a continuous stream ceases to produce any output values.</p>
      </section>
      <section>
        <h3><a name="components">UI Components as State-Machines</a></h3>
        <p>Since components are the reactive stream-processing elements of this application, this means that they're implemented using <code>Wire</code>s.</p>
        <p>As alluded to above, the components used to create the UI exhibit different behaviors, and for many of them their behaviors can change over time, e.g., depending on whether the user is interacting with the component or not.</p>
        <p>In more sophisticated cases, this requires components to be able to keep track of and change <em>state</em> in some way, which means that our <code>Wire</code>s need to be able to do the same.</p>
        <p>Netwire provides a few ways to do this sort of thing, so it took me a while to determine the best approach for this application. What I ended up using was the <em>delayed switch</em> function:</p>
        <code>
          <pre>
dSwitch :: Monad m =&gt; Wire m i (o, Event (Wire m i o)) -&gt; Wire m i o
          </pre>
        </code>
        <p>This function takes a <code>Wire</code> as an argument that it uses to process an input stream, emitting the regular stream-output values issued by the wire as its own outputs. It does this up until there is an <code>Event</code> output from the wire, after which it switches to behave like the <code>Wire</code> carried as the event payload.</p>
        <p>Using this function I was able to divide components into discrete operating states and switch between them in response to user input events, yielding different behaviors.</p>
        <p>To facilitate this, a convenience wrapper was written around <code>dSwitch</code>, named <code>mealyState</code> <sup><a name="ref2ret" href="#ref2">2</a></sup>. This function takes two functions as arguments: one to produce the stream output for the state and one to produce a new state to transition to, when appropriate. Both argument functions are run on values from the input stream to create the output <code>dSwitch</code> expects:</p>
        <code>
          <pre>
mealyState :: Monad m =&gt; (i -&gt; o) -&gt; (i -&gt; Event (Wire m i o)) -&gt; Wire m i o
mealyState outf transf = dSwitch (arr (\i -&gt; (outf i, transf i)))
          </pre>
        </code>
        <p>Using this wrapper function each state of a component then consists of at least three pieces: a value representing the state, an output function, and a transition function.</p>
        <p>Here's an example state from the main waveform editing component:</p>
        <code>
          <pre>
normalIdle :: Monad m =&gt; EditScreenPanel m
normalIdle = mealyState normalIdleOutput normalIdleTransition

normalIdleOutput context =
  UIOutput
  { feedbackOf = ...  <em>-- feedback for updating the Model</em>
  , generatorOf = ... <em>-- function for generating output for the View</em>
  }

normalIdleTransition context =
           (combinedCtrlKeyEvent Down context              $&gt; adjustmentIdle)
  `mergeL` (keyEvent MetaAlt Down context                  $&gt; precisionIdle)
  `mergeL` (buttonInBoundsEvent b LeftButton Down context  $&gt; normalPrimed)
  `mergeL` (buttonInBoundsEvent b RightButton Down context $&gt; normalOperationPrimed)
  where
    b = boundsIn context
          </pre>
        </code>
        <p><code>normalIdle</code> (a wire) is the state itself; <code>normalIdleOutput</code> is the output function and <code>normalIdleTransition</code> is the transition function.</p>
        <p>The output function emits the output stream values of a component and is component-dependent, so we won't discuss the particulars here, but the form of the transition function is pretty consistent in this codebase and I'd like to touch upon what it's doing.</p>
        <p>The code of the transition function here is organized like a table, with mappings from an input event (on the left of each <code>$&gt;</code>) to the state that should be gone to when that event occurs (on the right).</p>
        <p>What's really happening here is the events emitted by the functions on the left are having their payloads overwritten with the <code>Wire</code>s on the right by the <code>$&gt;</code> Functor operator, thus becoming the <code>Wire</code>-carrying events needed to prompt a transition in the <code>dSwitch</code> function. These events are merged together by the calls to the <code>mergeL</code> netwire function to give the final transition event returned by the transition function, if any.</p>
        <p>By organizing the components into states like this some pretty complex behavior can be implemented while still keeping the code decently manageable.</p>
      </section>
      <section>
        <h3><a name="data-streams">Data Streams</a></h3>
        <p>At the bottom of all of these layers are the data streams.</p>
        <p>To review, the UI network's input stream consists of user input from the View and the state of the Model, and the output stream contains a description of what to display for the View and updates to run on the Model.</p>
        <p>While some aspects of the transformation from the former pair into the latter is component-dependent, there are still other general aspects of it that I would like to talk about.</p>
        <p>First, the input side.</p>
        <p>In my initial approach to processing the user input from the View I was running filters in many of the components to ramify the input data into different streams, to keep track of things like meta-key states, mouse position, mouse-button states, etc.</p>
        <p>This worked, but it was suboptimal: much of the same code was repeated across multiple components since the components needed to keep track of similar things, meaning there was duplicate processing taking place, and the code for it was slow because it was using the <code>proc</code> syntax, which I'd read currently desugars to less-than-optimal arrow code -- some experimenting with <a href="https://hackage.haskell.org/package/arrowp-qq" target="_blank">arrow-qq</a> in a branch confirmed this.</p>
        <p>So, some refactoring took place.</p>
        <p>Now, a single stage of input processing is done in the <code>trackPanelContext</code> arrow which records all of the relevant positions, sizes, and key/button states the components need into a single object that gets fed to the UI network as an input context. This helped simplify things and improved performance for the user-interface.</p>
        <p>The Model state is added to this context as well and is carried through the UI network pretty-much unmodified.</p>
        <p>Now for the output side.</p>
        <p>In the last post it was mentioned that update messages for the Model changed from being distinct message constructors to just being function calls. This was a boon when it came to combining the output streams of components together because all that had to be done to combine the Model update portion was to compose the functions together, meaning multiple updates can be generated by the UI and applied all at the same time. If no update is produced by a component, the identity function can be returned/used.</p>
        <p>The output for the View is a tree representing the layout of the UI. (This is what gets generated after the delay by the function that's produced by the UI operation and applied to the Model's state.) The structure of this tree is determined by how the components are combined in the UI network (by the <code>layeredPanel</code> and <code>splitPanel</code> functions), and the contents of the tree (which itself is generic) are objects produced by the components that tell the View what to draw for each particular region of the screen.</p>
        <p>Thus the output for the View is generated each frame, rather than continuously existing and being updated whenever something changes, which means it's probably not as efficient as it could be.</p>
      </section>
      <section>
        <h2>Analysis</h2>
        <p>This setup does a pretty good job, but certain aspects of it cause it not to be as as flexible as a regular GUI framework, and other things just strike me as code smells; I thought I'd take a moment to discuss these things here.</p>
        <h3>External Modification</h3>
        <p>Currently, modifications to the UI cannot feasibly be made by an external actor. The layout can't be changed, and component behaviors cannot be swapped-out, added, or removed. This is because the structure of the UI is determined by the structure of the FRP network.</p>
        <p>All changes to the network are currently affected either within it or by replacing it completely. Messages to update it could be sent in as a sort of input, but that seems like an ill-advised way to make modifications, as does attempting to manipulate the structure of the network itself (which should be possible since the constructors of the <code>Wire</code> type appear to be public).</p>
        <p>The layout tree produced by the network could be transformed after generation, but then it would be out of sync with the behavior of the network.</p>
        <p>Maybe with a different FRP library it'd be possible to make changes from the outside, or perhaps using something crazy-fancy with lenses or zippers, but I don't know that there's a simple solution at the moment, which means that this method of coding a UI is pretty static.</p>
        <h3>Events</h3>
        <p>Those who've done GUI programming in other environments like web front-ends will be used to how user interactions flow through the UI in <em>events</em>. The reactive environment feels similar to this, but more limited since it's just inputs-to-outputs.</p>
        <p>Often events in a GUI framework will have different <em>phases</em>, such as "capture" and "bubble", and will have other features like the ability to be <em>cancelled</em>, but to have that in a reactive environment you'd need to implement it on top of everything else. It feels like such analogs should be possible in FRP, but I've not spent the time yet to research them.</p>
        <h3>Knowledge of the Outside World</h3>
        <p>Components in this framework know where they are located on the screen absolutely, rather than being positioned relatively, and that feels like it might be giving them too much knowledge of their environment.</p>
        <p>Contrapositively, the way things are written now the components are not aware of mouse movement outside of their bounds, which I could see being useful for them to know (e.g. when a user is interacting with a component and accidentally strays from its bounds, but didn't mean to, it might be nice for the component to keep on working).</p>
        <h3>How it is to Use</h3>
        <p>Despite having some detractive qualities, this technique feels pretty nice to program in: the compositional nature of assembling components together makes it feel like I'm using a specification language for the layout, and using state-machines for implementing the components makes coding them feel fairly clear.</p>
      </section>
    </article>
    <footer>
      <hr/>
      <nav>
        <a href="2021-07-08_architectural_refinements.html">&#171; Architectural Refinements</a>
        <a href="index.html">Index</a>
        <a href="2022-02-07_optimization.html">Optimization &#187;</a>
      </nav>
      <hr/>
      <section>
        <h1>Footnotes</h1>
        <ol>
          <li><a name="ref1"></a>Apparently <a href="https://www.reddit.com/r/haskell/comments/7yezo2/til_netwire_frp_library_is_basically_deprecated/" target="_blank">the netwire library has been deprecated</a> in favor of the <a href="https://hackage.haskell.org/package/wires" target="_blank">wires</a> library, but I was unable to get the latter to install with <code>stack</code> so I used the former.<br/><a href="#ref1ret">Return to text &#8593;</a></li>
          <li><a name="ref2"></a>I've named this function <code>mealyState</code> since it represents a state of a state-machine where the output is derived from both the inputs and the current state, as in a <a href="https://en.wikipedia.org/wiki/Mealy_machine" target="_blank">Mealy machine</a>.<br/><a href="#ref2ret">Return to text &#8593;</a></li>
        </ol>
      </section>
    </footer>
  </body>
</html>
